package com.cerealkillers.getposttest;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Risto on 5/28/2016.
 */
public class DownloadManager {
    private static DownloadManager instance = new DownloadManager(); //prvo se pravi jer je static
    private OkHttpClient client = new OkHttpClient();
    private IThreadWakeUp wakeUp; //setuje WakeUp na instancu MainActivity klase, moze da poziva samo implementiranu funkciju
    private DownloadManager()
    {

    }
    public static DownloadManager getInstance()
    {
        return instance;
    }

    public void getData()
    {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run()
            {
                try {
                    Request request = new Request.Builder()
                            .url("http://cookbuddy.softelm.com/recepti")
                            .build();

                    Response response = client.newCall(request).execute();
                    String s = response.body().string();
                    wakeUp.ResponseOk(s);
                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public void postData()
    {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run()
            {
                try {
                    RequestBody formBody = new FormBody.Builder()
                            .add("namirnice[]", "sunka")
                            .add("namirnice[]", "sir")
                            .build();
                    Request request = new Request.Builder()
                            .url("http://cookbuddy.softelm.com/search")
                            .post(formBody)
                            .build();

                    Response response = client.newCall(request).execute();
                    String s = response.body().string();
                    wakeUp.ResponseOk(s);

                }
                catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public void setThreadWakeUp(IThreadWakeUp i)
    {
        wakeUp = i; //tu se setuje referenca

    }
}
