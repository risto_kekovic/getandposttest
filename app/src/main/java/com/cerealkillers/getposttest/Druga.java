package com.cerealkillers.getposttest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Druga extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_druga);
        this.mContext = this;
        Intent intent = getIntent();
        String bla = intent.getExtras().getString("podaci");
        //parse JSON
        RelativeLayout[] recepti;
        final JSONObject[] elementi;
        TextView[] nazivi;
        TextView[] vremena;
        TextView[] tezine;
        JSONObject reader;
        JSONArray potpuno;
        try
        {
            reader = new JSONObject(bla);
            potpuno  = reader.getJSONArray("potpuno");
            elementi = new JSONObject[potpuno.length()];
            nazivi = new TextView[potpuno.length()];
            vremena = new TextView[potpuno.length()];
            tezine = new TextView[potpuno.length()];
            recepti = new RelativeLayout[potpuno.length()];
            elementi[0] = potpuno.getJSONObject(0);
            String naziv = elementi[0].getString("naziv");
            String vreme = elementi[0].getString("vreme");
            String tezina = elementi[0].getString("tezina");
            RelativeLayout rl = (RelativeLayout) findViewById(R.id.container);
            TextView pp = new TextView(mContext);
            pp.setBackgroundColor(Color.parseColor("#31F14c"));
            pp.setText("Potpuno poklapanje");
            pp.setId(View.generateViewId());
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            rl.addView(pp, params);


            recepti[0] = new RelativeLayout(mContext);
            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.BELOW, pp.getId());
            //recepti[0].setBackgroundColor(Color.GREEN);
            recepti[0].setId(View.generateViewId());
            rl.addView(recepti[0], params);
            recepti[0].setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    Intent inte = new Intent("android.intent.action.TRECA");
                    String s = elementi[0].toString();
                    inte.putExtra("podaci", s);
                    startActivity(inte);
                }
            });

            nazivi[0] = new TextView(mContext);
            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            nazivi[0].setText(naziv);
            nazivi[0].setTextSize(20);
            nazivi[0].setId(View.generateViewId());
            recepti[0].addView(nazivi[0], params);

            vremena[0] = new TextView(mContext);
            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.BELOW, nazivi[0].getId());
            vremena[0].setText("Vreme pripreme: " + vreme + " minuta");
            vremena[0].setId(View.generateViewId());
            recepti[0].addView(vremena[0], params);

            tezine[0] = new TextView(mContext);
            params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.BELOW, vremena[0].getId());
            tezine[0].setText("Tezina: " + tezina);
            tezine[0].setId(View.generateViewId());
            recepti[0].addView(tezine[0], params);

            for(int i=1; i<potpuno.length(); i++)
            {
                elementi[i] = potpuno.getJSONObject(i);
                naziv = elementi[i].getString("naziv");
                vreme = elementi[i].getString("vreme");
                tezina = elementi[i].getString("tezina");
                rl = (RelativeLayout) findViewById(R.id.container);

                recepti[i] = new RelativeLayout(mContext);
                params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, recepti[i - 1].getId());
                recepti[i].setId(View.generateViewId());
                rl.addView(recepti[i], params);
                final int br = i;
                recepti[i].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent inte = new Intent("android.intent.action.TRECA");
                        String s = elementi[br].toString();
                        inte.putExtra("podaci", s);
                        startActivity(inte);
                    }
                });

                nazivi[i] = new TextView(mContext);
                params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                nazivi[i].setText(naziv);
                nazivi[i].setTextSize(20);
                nazivi[i].setId(View.generateViewId());
                recepti[i].addView(nazivi[i], params);

                vremena[i] = new TextView(mContext);
                params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, nazivi[i].getId());
                vremena[i].setText("Vreme pripreme: " + vreme + " minuta");
                vremena[i].setId(View.generateViewId());
                recepti[i].addView(vremena[i], params);

                tezine[i] = new TextView(mContext);
                params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, vremena[i].getId());
                tezine[i].setText("Tezina: " + tezina);
                tezine[i].setId(View.generateViewId());
                recepti[i].addView(tezine[i], params);
            }
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_druga, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
