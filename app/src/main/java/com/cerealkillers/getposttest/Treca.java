package com.cerealkillers.getposttest;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class Treca extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treca);
        this.mContext = this;
        Intent intent = getIntent();
        String bla = intent.getExtras().getString("podaci");
        try
        {
            JSONObject element = new JSONObject(bla);
            String naziv = element.getString("naziv");
            String vreme = element.getString("vreme");
            String tezina = element.getString("tezina");
            String opis = element.getString("opis");
            TextView tv = (TextView)findViewById(R.id.nazivRecepta);
            tv.setText(naziv);
            tv = (TextView) findViewById(R.id.vremePripreme);
            tv.setText("Vreme pripreme: " + vreme + " minuta");
            tv = (TextView) findViewById(R.id.tipJela);
            tv.setText("Tezina: " + tezina);
            tv = (TextView) findViewById(R.id.textRecepta);
            tv.setText(opis);
        }
        catch(JSONException ec)
        {
            ec.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_treca, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
