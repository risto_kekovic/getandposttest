package com.cerealkillers.getposttest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements IThreadWakeUp { //kompozitni objekat, 3 objekta u sebi



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DownloadManager.getInstance().setThreadWakeUp(this); //referenca na ovaj objekat se nalazi u Download Manager
        //DownloadManager.getInstance().getData();
        DownloadManager.getInstance().postData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void ResponseOk(String s) //on ceka da se thread zavrsi odnosno da dobije podatke sa servera
    {
        
        if(s.isEmpty())
        {
            //nije dobio podatke, treba uraditi nesto
			//treba probati jos jednom da se pribave podaci, ako je doslo do greske, ponovo se poziva DownloadManager.getData
			//ako nije ni tada, onda treba nekako obezbediti da ne pukne aplikacija
			//ispisati poruku da je doslo do greske na serveru, to samo ako 2 puta ne dobijemo nista
			//promenljiva koja to obezbedjuje
        }
        else
        {
            //dobio je podatke
			Intent i = new Intent(this, Druga.class); //communication between activities
			i.putExtra("podaci",s);
			startActivity(i);
        }
    }
}
